<?php

namespace Tin\Test\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @see TestService
 */
class Test extends Facade
{
    protected static function getFacadeAccessor()
    {
        return "test";
    }
}
