<?php

namespace Tin\Test;

use Illuminate\Support\ServiceProvider;

class TestProvider extends ServiceProvider
{
    public function boot(){
        $this->publishes([
            __DIR__.'/config/test.php'=>config_path("test.php"),//发布配置文件
        ]);
    }

    public function register()
    {

        $this->app->singleton('test',function(){
            return new TestService();
        });
    }
}
